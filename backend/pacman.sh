#######################
# Backend for PKGBACK and maybe others later
#######################
printf "\e[0;33mUsing pacman...\e[0;0m\n"

function _private_setup()
{
    _wrks_setup " --preview=\"pacman -Si {}\" --bind \"enter:execute(bash -c 'pacman $1 --hookdir /etc/pacman.d/hooks --hookdir $DIR/backend/pachooks --noconfirm {+}')+accept\""
}

function _private_setup_noact()
{
    _wrks_setup_noact " --preview=\"pacman -Si {}\""
}

function _wrks_add()
{
    _wrks_mkflag
    _private_setup "-Syu"
    $DIR/backend/pacman-helper pacman | fzf
    _wrks_wait_for_child "pkg add"
}

function _wrks_rm()
{
    _wrks_mkflag
    _private_setup "-Rs"
    pacman -Qqs ".*" | fzf
    _wrks_wait_for_child "pkg rm"
}

function _wrks_explore()
{
    _wrks_setup " --preview=\"pacman -Qli {}\" --bind \"enter:execute(man {})\""
    pacman -Qqs ".*" | fzf
}

function _wrks_execute_replace()
{
    local torem=$1
    local toinst=$2

    _wrks_elevated "pacman -Rsd $torem" "pacman -Syu $toinst"

    echo "Package replace executed. Please check if dependencies are still consistent!"
}

function _wrks_replace()
{
    _private_setup_noact
    local pkga=$(pacman -Qqs '.*' | fzf)
    local pkgb=$($DIR/backend/pacman-helper | fzf)
    _wrks_confirm "Replace '$pkga' with '$pkgb'" && _wrks_execute_replace $pkga $pkgb || echo "Replace canceled"
}

function _wrks_up()
{
    _wrks_elevated "pacman -Syu"
}

function _wrks_pending()
{
    _wrks_setup " --preview=\"pacman -Qci {}\""
    pacman -Sy
    pacman -Qu | less
}

function _wrks_mirrors()
{
    if grep "Manjaro" /etc/lsb-release &> /dev/null; then
        echo "Auto generating mirror list based of your geoip..."
        _wrks_elevated 'pacman-mirrors --api --protocol https --geoip' 'pacman -Syyuu'
    else
        local base_url='https://www.archlinux.org/mirrorlist'
        local https='n'
        local ipv6='n'

        _wrks_setup ' --prompt="From where do you want your packages? > " --no-sort --tac --cycle'
        SELECT=$(cat "$DIR/backend/country.json" | $DIR/backend/queryj values --name Name | fzf)
        CCODE=$(cat "$DIR/backend/country.json" | $DIR/backend/queryj key_query "Code" "Name=$SELECT")
        echo "$CCODE > $SELECT"
        dialog --yesno "Do you want to use HTTPS?" 10 20
        [ $? -eq 0 ] && https='y'
        dialog --yesno "Do you want to use IPv6?" 10 20
        [ $? -eq 0 ] && ipv6='y'
        echo '==== SUMMARY ===='
        echo " > Location: $SELECT ($CCODE)"
        echo " > HTTPS: $https"
        echo " > IPv6: $ipv6"
        echo ''
        if _wrks_confirm "OK?"; then
            local endpoint="/?country=$CCODE&protocol=http&ip_version=4&use_mirror_status=on"
            [ "$https" = "y" ] && endpoint="$endpoint&protocol=https"
            [ "$ipv6" = "y" ] && endpoint="$endpoint&ip_version=6"
            echo "Creating mirrorlist backup"
            local datestamp=$(date --iso)
            cp -vf /etc/pacman.d/mirrorlist $HOME/pacman.mirrorlist_$datestamp.bak
            echo "Fetching list from $base_url$endpoint and rebuilding index"
            _wrks_elevated "curl -s "$base_url$endpoint" | sed -e 's/^#Server/Server/' > /etc/pacman.d/mirrorlist" 'pacman -Syyuu'
        else
            exit 1
        fi
    fi
}

function _wrks_files()
{
    _wrks_setup " --preview=\"pacman -Si {}\""
    local selection=$(pacman -Qqs ".*" | fzf)
    _wrks_setup
    pacman -Ql $selection | sed "s/$selection //g" | fzf
}


export PKGBACK=pacman
