#######################
# Common functions for all backends
#######################

source $DIR/backend/colors/gruvbox.conf
source $DIR/settings.conf

PKG_FLAG='/dev/shm/pkgflag'
PACMAN_LOCK='/var/lib/pacman/db.lck'


_wrks_panic()
{
    printf "\n\n\e[0;31m\tPKG PANIC: \e[0;21m$1\n\n"
    exit 1
}

_wrks_elevated() {
    local cmd_str=""
    local execusr=$USER
    local logfile=$(mktemp)
    for cmd in "$@"
    do
        cmd_str="$cmd_str;$cmd &>> $logfile"
    done
    if [ $UID -ne 0 ]; then
        bash -c "chown root:root $logfile$cmd_str;chown $execusr:$execusr $logfile" # Let's do the annoying thing and use pkexec!
    else
        cmd_str=$(echo "$cmd_str" | cut -c 2-)
        $cmd_str # We are already root, better not anger the admins
    fi
    cat $logfile
}

_wrks_noop() {
    echo "Not implemented! :("
    exit 1
}

_wrks_confirm() {
    display_str=$1
    default_ans='y/N'
    if [[ $default_ans == 'y/N' ]]
    then
        must_match='[yY]'
    else
        must_match='[nN]'
    fi
    read -p "${display_str} [${default_ans}]:" ans
    [[ $ans == $must_match ]]
}

_wrks_mkflag()
{
    echo $(date +%s) > /dev/shm/pkgflag
}

_wrks_wait_for_child()
{
    local orgcmd=$1
    while true
    do
        if [[ ! -e $PACMAN_LOCK ]] && [[ -e $PKG_FLAG ]]; then
            rm -f $PKG_FLAG
            _wrks_confirm "Your package manager seems to encountered an error. Try again?" && exec $orgcmd || exit 1
        elif [[ ! -e $PACMAN_LOCK ]] && [[ ! -e $PKG_FLAG ]]; then
            echo "Received OK signal"
            break
        fi
        sleep 1
    done
}

_wrks_setup()
{
    PKGLOG='/tmp/wrks.log'
    [[ ! -e $PKGLOG ]] && touch $PKGLOG
    INFO_HEADER="<ESC> Exit | <PgUp> Jump ↑| <PgDown> Jump ↓| <Shift+Up> Preview ↑| <Shift+Down> Preview ↓"
    FZF_OPTS="$FZF_DEFAULT_OPTS -m --bind \"f8:execute(dialog --backtitle 'Pkgwrk' --title 'Last log file' --textbox '$PKGLOG' 0 0)\" --cycle --border --layout=reverse-list --preview-window=right:wrap --header \"$INFO_HEADER - $PKGBACK\""
    export FZF_DEFAULT_OPTS="$FZF_OPTS:$1"
}

_wrks_setup_noact()
{
    INFO_HEADER="<ESC> Exit | <PgUp> Jump ↑| <PgDown> Jump ↓| <Shift+Up> Preview ↑| <Shift+Down> Preview ↓"

    FZF_OPTS="$FZF_DEFAULT_OPTS -m --cycle --border --layout=reverse-list --preview-window=right:wrap --header \"$INFO_HEADER\""
    export FZF_DEFAULT_OPTS="$FZF_OPTS:$1"
}
