#######################
# Backend for dpkg
#######################

printf "\e[0;33mUsing apt\\dpkg...\e[0;0m\n"

function _private_setup()
{
    local out=""
    echo $out
}

function _wrks_add()
{
    apt-get update  # Let's keep the index fresh
    local sel_pkgs=$(apt-cache pkgnames | sort | fzf --preview='apt-cache show {+}')
    _wrks_rootguard
    apt-get install -y $sel_pkgs
}

function _wrks_rm()
{
    _private_setup "purge -y"
    _wrks_rootguard
    dpkg-query --showformat='${Package}\n' -W '*' | fzf
}

function _wrks_up()
{
    _wrks_rootguard
    apt-get update && apt-get dist-upgrade
}

function _wrks_pending()
{
    _wrks_rootguard
    _wrks_setup " --preview=\"apt-get changelog {}\""
    apt-get update && apt-get -u | fzf
}

function _wrks_files()
{
    _wrks_noop
}

function _wrks_mirrors()
{
    _wrks_noop
}

function _wrks_explore()
{
    _wrks_noop
}

function _wrks_replace()
{
    _wrks_noop
}

