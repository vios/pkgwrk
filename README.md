# pkgwrk

Package Work aka pkgwrk is a simple TUI for yay. 

Pair it with fd and you can search for Arch Linux packages in a breeze while not having to deal with a complicated interface.

This script is not done yet. Even though I'm 80% there there are a few things things missing that would make the user experience even better:

- [ ] Make it clear how to exit the application. (You can do it with ESC but the application doesn't tell you that)
- [ ] Filter already installed packages from the install screen.
- [ ] Remove the extra AUR script. We are using yay afterall and should be able to access the AUR the same way we are accessing the normal repositories
- [ ] Testing especially the part how I set the fzf theme. I would want to avoid user settings.
- [ ] No yay? No Problem! The script already can work with pacman only and we could support both.
- [ ] Add more commands. Right now there is only install and remove.. which might be almost all we need but for going 100% a direct install command and direct update command would be nice
- [ ] Improve on Design. It's almost barebones fzf. Maybe we can do better.
